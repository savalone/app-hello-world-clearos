# "Hello World" app for ClearOS panel

This is just an example app for ClearOS panel. ClearOS apps are CodeIgniter modules. So, learning how to create ClearOS apps is almost the same as learning CodeIgniter. :)


## Reading this README

Any block with monospaced fonts is a set of commands. Inside these block, any line starting with `#` is a command to be run as root user. Any line starting with `$` is a command to be run as a regular user (**rdccoder** in this example).

The username **rdccoder** in this example can be replaced by any other string. You will avoid troubles if the string you choose match the regex `^[a-zA-Z]\w{5,}$`.

The password **tikiwiki** can also be replaced by any other string. The recommendation is the almost the same, but matching `^[:print:]{8,}$`.


## Advices

Always use PHP version 5.4 while coding, because it is probable any other ClearOS instance will run the created with this obsolete PHP version.


## Setting up dev environment

The official documentation[1] instructs to install php development tools. The steps below provides all packages to get a very simple app.

1. Create a regular user
```
# useradd -m -U -G wheel -u 1000 rdccoder
```

2. Enable dev respositories
```
# yum-config-manager --enable clearos-developer,clearos-centos,clearos-centos-updates,clearos-epel
```

3. Make the system up to date (grab a coffee)
```
# yum upgrade
```

4. Install tools we need to build apps
```
# yum install clearos-devel app-devel
```

5. Run setup tool
```
# clearos setup
```

Fill the questions with

| Question                   | Value        |
|----------------------------|--------------|
| username (e.g. david)      | rdccoder        |
| password                   | tikiwiki     |
| editor (e.g. /usr/bin/vim) | /usr/bin/vim |


6. Check permissions

A system user called (webconfig) will try to read the home folder of user we created before, make sure it is readable. Don't worry with security for now. 

```
# chmod 755 /home/rdccoder
```


## Creating an app

1. Switch to regular user created before
```
# su rdccoder
$ cd
```

2. Create a folder to host all apps you will create and change to it
```
$ mkdir ~/apps
$ cd ~/apps
```

3. Let's tell webconfig where to get our apps
```
cat > ~/.clearos <<'EOF'
<?php
// this is relative to user home folder
\clearos\framework\Config::$apps_paths[] = 'apps';
EOF
```

4. User `clearos` command to bootstrap a new app called 'hello_world'
```
$ clearos newapp
```

When a prompt appears, answer questions with these values:

| question | value       |
|----------|-------------|
| basename | hello_world |
| fullname | Hello World |
| type     | git         |

After that, a folder called `hello_world` will be created with a CodeIgniter module skeleton.

4. Loading the app on ClearPanel

The command below will be executed as the regular user **rdccoder**, but you will see the effect while logged in as **root** on ClearOS webpanel at URL like https://192.168.56.102:1501/app/hello_world. 

**IMPORTANT**: The port is **1501**, not **81**! [2]

```
$ clearos reload
```

[1] https://www.clearos.com/clearfoundation/development/clearos/content:en_us:dev_framework_getting_started
[2] https://www.clearos.com/clearfoundation/development/clearos/content:en_us:dev_framework_tutorials_hello_world
