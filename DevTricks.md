# Dev tricks

## Installing VirtualBox Guest Additions

1. Click on  * Devices > Insert Guest Additions CD image*

2. Download image if it ask

3. Click insert CD

4. On ClearOS terminal as root, mount the CD
```
# mount /dev/sr0 /media
```

5. Install kernel-devel
```
# yum install -y kernel-devel
```

6. Run the installer
```
# cd /media
# ./VBoxLinuxAdditions.run
```

7. Reboot system
```
# reboot
```

## Mounting remote folder on local machine (VBox Share)

1. Install VirtualBox Guest Additions

2. Click on *Devices > Shared Folder > Shared Folder Settings*

3. Click on the "Add new shared folder"

4. On host, select the folder you want to map (eg. **/home/fabio/Work/avantech/clearapps**) and give it a name (eg.: **clearapps**)

5. Supposing your userid is 1000 and username is *rdccoder*, mount like this
```
# mount -t vboxsf -o uid=1000,gid=1000 clearapps /home/rdccoder/apps
```


## Mounting remote folder on local machine (NFS)

1. Install NFS server on host system
```
# apt-get install nfs-utils
```

2. Start the service on host
```
# systemctl start nfs-server
```

1. Setup a shared folder
```
# cat >> /etc/exports << EOF
/home/fabio/Work/avantech/clearapps    *(rw,sync,no_subtree_check,no_root_squash)
EOF
```

2. Reload the NFS exports
```
# exportfs -arv
```

** On ClearOS Virtual Machine **

3. Install nfs-utils
```
# yum install -y nfs-utils
```

4. Create an accessible target directory
```
# mkdir /home/rdccoder/apps
# chown -R rdccoder: /home/rdccoder/apps
```

5. Mount Host folder on ClearOS VM
```
# mount -t nfs -o vers=4 HOST_IP:/home/fabio/Work/avantech/clearapps /home/rdccoder/apps
```
